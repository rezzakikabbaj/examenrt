package com.example.examenrt;

public record MyData(
        Employee employee,
        OrderHeader orderHeader,
        OrderDetail orderDetail,
        Product product
) {
}
