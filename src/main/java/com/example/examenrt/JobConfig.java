package com.example.examenrt;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class JobConfig {
    @Bean
    @StepScope
    public FlatFileItemReader<MyData> csvDataFileReader(@Value("#{jobParameters['input.file']}") String inputFile) {
        return new FlatFileItemReaderBuilder<MyData>()
                .name("billingDataFileReader")
                .resource(new FileSystemResource(inputFile))
                .delimited()
                .names("myData.employee", "myData.orderHeader", "myData.orderDetail","myData.product")
                .targetType(MyData.class)
                .build();
    }
    @Bean
    @StepScope
    public FlatFileItemWriter<MyData> csvDataFileWriter(@Value("#{jobParameters['output.file']}") String outputFile) {
        return new FlatFileItemWriterBuilder<MyData>()
                .resource(new FileSystemResource(outputFile))
                .delimited()
                .names("myData.employee", "myData.orderHeader", "myData.orderDetail","myData.product")
                .build();
    }
    @Bean
    public CustomItemProcessor csvDataProcessor(JdbcTemplate jdbcTemplate,@Value("#{jobParameters['quer']}") String query) {
        return new CustomItemProcessor(jdbcTemplate, query);
    }

    @Bean
    public Step step1(JobRepository jobRepository, PlatformTransactionManager transactionManager,
                      ItemReader<MyData> csvDataFileReader, ItemProcessor<MyData,MyData>csvDataProcessor,
                      ItemWriter<MyData> csvDataFileWriter) {
        return new StepBuilder("lignesAltéres", jobRepository)
                .<MyData, MyData>chunk(100, transactionManager)
                .reader(csvDataFileReader)
                .processor(csvDataProcessor)
                .writer(csvDataFileWriter)
                .build();
    }
    @Bean
    public Job job(JobRepository jobRepository, Step step1){
        return new JobBuilder("lignes alters",jobRepository)
                .start(step1)
                .build();

    }
}
