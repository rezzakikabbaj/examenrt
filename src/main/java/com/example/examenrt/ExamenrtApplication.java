package com.example.examenrt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamenrtApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExamenrtApplication.class, args);
    }

}
