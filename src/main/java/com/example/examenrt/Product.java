package com.example.examenrt;

public record Product(int productId, String name, int productNumber) {
}
