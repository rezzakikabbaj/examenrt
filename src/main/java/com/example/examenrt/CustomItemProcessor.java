package com.example.examenrt;


import org.springframework.batch.item.ItemProcessor;
import org.springframework.jdbc.core.JdbcTemplate;

public class CustomItemProcessor implements ItemProcessor<MyData, MyData> {

        private final JdbcTemplate jdbcTemplate;
        private final String sqlFilterQuery;

        public CustomItemProcessor(JdbcTemplate jdbcTemplate, String sqlFilterQuery) {
            this.jdbcTemplate = jdbcTemplate;
            this.sqlFilterQuery = sqlFilterQuery;
        }

        @Override
        public MyData process(MyData item) throws Exception {
            if (isValidData(item)) {
                return item;
            } else {
                return null;
            }
        }

        private boolean isValidData(MyData item) {
            String query = String.format(sqlFilterQuery, item.getClass());
            Integer result = jdbcTemplate.queryForObject(query, Integer.class);

            return result != null && result > 0;
        }
    }


