select Distinct
    P.Name         as ProductName,
    P.Color        as Color,
    StandardCost,
    Size,
    OrderQty,
    LineTotal,
    OrderDate,
    TotalDue
FROM Production.Product P
    INNER JOIN
    Sales.SalesOrderDetail SOD ON P.ProductID = SOD.ProductID
    INNER JOIN
    Sales.SalesOrderHeader SOH ON SOD.SalesOrderID = SOH.SalesOrderID;